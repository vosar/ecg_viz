import time


STEP = 10

def write_to_pipe(data, step):
    for i in range(0, len(data), step):
        if i % 100 == 0: print(len(data) - i)
        p = open('/tmp/testpipe', 'w')
        slice = ','.join(data[i:i+step])
        p.write(slice)
        p.close()
        time.sleep(0.05)


if __name__ == "__main__":
    with open("client/public/data/ecg_oleg.csv") as f:
        ecg = f.read().split(',')

    write_to_pipe(ecg, STEP)
