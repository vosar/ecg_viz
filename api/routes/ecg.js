const fs = require('fs');
const net = require('net');
const FIFO = require('fifo-js');

var express = require('express');
var router = express.Router();
const fifo_path = "/tmp/testpipe";
const fifo = new FIFO(fifo_path);

router.get('/', function(req, res, next) {
  fifo.read(text => {
    // 'text' contains the text which was read from the fifo.
    res.send(text.toString());
  })
});

module.exports = router;