import React, { Component } from 'react';

import './Chart.scss';

import { Grid } from '@vx/grid';
import { Group } from '@vx/group';
import { curveBasis } from '@vx/curve';
import { GlyphDot } from '@vx/glyph';
import { AxisLeft, AxisBottom } from '@vx/axis';
import { Line, LinePath, Bar } from '@vx/shape';
import { scaleLinear } from '@vx/scale';
import { PatternLines } from '@vx/pattern';
import { Point } from '@vx/point';

import BEM from '../../helpers/BEM';

// var CanvasJS = require('canvasjs');

const b = BEM('Chart');

// accessors
const x = d => d.index;
const y = d => d.value;

// responsive utils for axis ticks
function numTicksForHeight(height) {
  if (height <= 300) return 3;
  if (300 < height && height <= 600) return 5;
  return 10;
}

function numTicksForWidth(width) {
  if (width <= 300) return 2;
  if (300 < width && width <= 400) return 5;
  return 10;
}

class Chart extends Component {
  static defaultProps = {
    rpeaks: [],
    visible: true,
  };
  constructor(props) {
    super(props);
    const axeSize = 70;
    const margin = { top: 5, right: 0, bottom: 0, left: 0 },
      width = window.innerWidth - axeSize * 2,
      height = 120;

    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);

    this.state = {
      width,
      height,
      axeSize,
      margin,
    };
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth - this.state.axeSize * 2 });
  }

  static reformatData(rawData) {
    let data = [];

    for (let i = 0; i < rawData.length; ++i) {
      data.push({ index: i, value: rawData[i] });
    }
    return data;
  }

  static reformatRpeaksData(rpeaks, rawData, startValue) {
    let data = [];

    for (let i = 0; i < rpeaks.length; ++i) {
      const idx = Math.abs(startValue - rpeaks[i]);
      data.push({ index: idx, value: rawData[idx] });
    }
    return data;
  }

  render() {
    const { width, height, axeSize, margin } = this.state;
    const {
      ecgData,
      title,
      startValue,
      endValue,
      sampleRate,
      rpeaks,
      yRange,
      visible,
    } = this.props;

    // bounds
    const xMax = width - margin.left - margin.right;
    const yMax = height - margin.top - margin.bottom;

    // scales
    const xScale = scaleLinear({
      range: [0, xMax],
      domain: [0, ecgData.length - 1],
    });
    const yScale = scaleLinear({
      range: [yMax, 0],
      domain: yRange,
      nice: true,
    });

    const data = Chart.reformatData(ecgData);
    const rpeaksData = Chart.reformatRpeaksData(rpeaks, ecgData, startValue);

    const sectionStyle = {
      width: width + axeSize + 50,
      height: height + axeSize + 32 + 20 * 2,
    };

    // colors
    const primary = '#8921e0';
    const secondary = '#00f2ff';
    const contrast = '#ffffff';
    let is_hidden = visible ? b() : b('hidden');
    // console.log(width);
    return (
      <section className={is_hidden} style={sectionStyle}>
        <h3 className={b('title')}>{title}</h3>
        <svg
          width={width + axeSize + 20}
          height={height + axeSize}
          className={b('svg')}
        >
          {/*<PatternLines*/}
          {/*  id="dhLines"*/}
          {/*  height={6}*/}
          {/*  width={6}*/}
          {/*  stroke="#bdc3c7"//"rgba(231, 76, 60, 0.5)"*/}
          {/*  strokeWidth={1}*/}
          {/*  orientation={['vertical', 'horizontal']}*/}
          {/*/>*/}
          <Bar
            top={margin.top}
            fill={`url(#dhLines)`}
            height={height}
            width={width}
            x={axeSize}
            y={0}
            rx={0}
            className={b('bar')}
          />
          <Grid
            top={margin.top}
            left={axeSize}
            xScale={xScale}
            yScale={yScale}
            stroke="#95a5a6"//"rgba(142, 32, 95, 0.9)"
            width={xMax}
            height={yMax}
            numTicksRows={numTicksForHeight(height)}
            numTicksColumns={numTicksForWidth(width)}
          />
          <Group top={margin.top} left={axeSize}>
            {/*<Line*/}
            {/*  strokeWidth={3}*/}
            {/*  stroke={'#7f8c8d'}*/}
            {/*  from={*/}
            {/*    new Point({*/}
            {/*      x:*/}
            {/*        (width * startValue) /*/}
            {/*        (totalECGLength - endValue + startValue),*/}
            {/*      y: 0,*/}
            {/*    })*/}
            {/*  }*/}
            {/*  to={*/}
            {/*    new Point({*/}
            {/*      x:*/}
            {/*        (width * startValue) /*/}
            {/*        (totalECGLength - endValue + startValue),*/}
            {/*      y: height,*/}
            {/*    })*/}
            {/*  }*/}
            {/*/>*/}
            <LinePath
              data={data}
              x={d => xScale(x(d))}
              y={d => yScale(y(d))}
              stroke={'#2980b9'}
              strokeWidth={1}
              curve={curveBasis}
            />
            {/*{rpeaksData.map((d, i) => {*/}
            {/*  const cx = xScale(x(d));*/}
            {/*  const cy = yScale(y(d));*/}
            {/*  return (*/}
            {/*    <g key={`line-point-${i}`}>*/}
            {/*      <GlyphDot*/}
            {/*        cx={cx}*/}
            {/*        cy={cy}*/}
            {/*        r={4}*/}
            {/*        fill={contrast}*/}
            {/*        stroke={secondary}*/}
            {/*        strokeWidth={5}*/}
            {/*      />*/}
            {/*      <GlyphDot*/}
            {/*        cx={cx}*/}
            {/*        cy={cy}*/}
            {/*        r={4}*/}
            {/*        fill={secondary}*/}
            {/*        stroke={primary}*/}
            {/*        strokeWidth={1}*/}
            {/*      />*/}
            {/*      <GlyphDot cx={cx} cy={cy} r={2} fill={contrast} />*/}
            {/*    </g>*/}
            {/*  );*/}
            {/*})}*/}
          </Group>
          <Group left={margin.left}>
            <AxisLeft
              top={margin.top}
              left={axeSize}
              scale={yScale}
              hideZero
              numTicks={numTicksForHeight(height)}
              label="Amplitude (dB)"
              stroke="#1b1a1e"
              tickStroke="#8e205f"
              labelProps={{
                textAnchor: 'middle',
                fontFamily: 'Arial',
                fontSize: 12,
                fill: 'black',
              }}
            />

            <AxisBottom
              top={height}
              left={axeSize}
              scale={scaleLinear({
                range: [0, xMax],
                domain: [startValue / sampleRate, endValue / sampleRate],
              })}
              numTicks={numTicksForWidth(width)}
              label="Time (s)"
              labelProps={{
                textAnchor: 'middle',
                fontFamily: 'Arial',
                fontSize: 12,
                fill: 'black',
              }}
            />
          </Group>
        </svg>
      </section>
    );
  }
}

export default Chart;
