import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './App.scss';
import '../Chart';
import { csvParse } from 'd3';
import Chart from '../Chart';

import BEM from '../../helpers/BEM';

const b = BEM('App');

class App extends Component {
  state = {
    ecgData: [],
    sampleRate: null,
    filtered: [],
    rpeaks: null,
    start: 0,
    zoomValue: 5000,
    apiResponse: "",
    interval: null,
    showDenoised: false
  };

  constructor(props) {
    super(props);
    const id = 1;
    this.state.sampleRate = 512;
    // this.getRawECG(id);
    // this.getFilteredECG(id);
    this.getRpeaks(id);
  }

  callAPI() {
    fetch("http://localhost:9000/ecg")
      .then(res => res.text())
      .then(res => res.split(',')
          .map(parseFloat)
            .filter(value => !isNaN(value)))
      .then(data => this.setState({ecgData: this.state.ecgData.concat(data), start: Math.max(this.state.ecgData.length - this.state.zoomValue, 0)}));
    // console.log(this.state.ecgData.length);
    // if (this.state.ecgData.length > 5120){
    //   this.setState({start: this.state.start + 10});
    // }
  }

  componentDidMount() {
    const setTimer = window.setInterval(() => {
      this.callAPI()
    }, 50);
    this.setState({ interval: setTimer })
  }

  getRawECG = async id => {
    const response = await fetch('data/ecg_oleg.csv');
    const rawData = await response.text();

    const parseData = csvParse(rawData);
    let data = parseData.columns.map(parseFloat);//.filter(value => !isNaN(value));
    this.setState({ ecgData: data });
  };

  getFilteredECG = async id => {
    const response = await fetch('data/denoised_ecg_oleg_skips.csv');
    const rawData = await response.text();

    const parseData = csvParse(rawData);
    let data = parseData.columns
      .map(value => Number(value).toFixed(2))
      .map(parseFloat);
      // .map(value => value == 0.0 ? null:value);
    // console.log(data.slice(0, 20));
    // console.log(data.slice(190, 220));
      //.filter(value => !isNaN(value));
    this.setState({ filtered: data });
  };

  getRpeaks = async id => {
    const response = await fetch('data/rpeaks' + id.toString() + '.csv');
    const rawData = await response.text();

    const parseData = csvParse(rawData);
    let data = parseData.columns.map(parseFloat).filter(value => !isNaN(value));
    this.setState({ rpeaks: data });
  };

  updateECg = async (ecgSignal, zoomValue) => {
    for (let i = 0; i < ecgSignal.length - zoomValue; i=i+25) {
      this.setState({start: i});
      await sleep(0.05);
    }
  };

  setDefaultState = () => {
    this.setState({ecgData: [], filtered: [], start: 0, showDenoised:false});
  };

  render() {
    if (!this.state.ecgData)
      return <div className="App"> Loading raw ECG data...</div>;
    if (!this.state.filtered)
      return <div className="App"> Loading filtered ECG data...</div>;
    if (!this.state.rpeaks)
      return <div className="App"> Loading rpeaks indices...</div>;
    if (!this.state.sampleRate)
      return <div className="App"> Loading sample rate value...</div>;
    if (!this.state.zoomValue)
      return <div className="App"> Loading zoomValue...</div>;

    const {
      ecgData,
      filtered,
      rpeaks,
      sampleRate,
      zoomValue,
      start,
      showDenoised,
    } = this.state;

    const rawECGYRange = [Math.min(...ecgData), Math.max(...ecgData)];
    const filteredECGYRange = [Math.min(...filtered), Math.max(...filtered)];

    return (
      <main className={b()}>
        <form className={b("right")}>
          <input
            className={b('button')}
            type="button"
            value={'Restart'}
            onClick={() => {
              this.setDefaultState()
            }}
          />
        </form>
        <Chart
          ecgData={ecgData.slice(start, start + zoomValue)}
          sampleRate={sampleRate}
          startValue={start}
          endValue={start + zoomValue}
          yRange={rawECGYRange}
          title={'Recorded raw signal'}
          totalECGLength={ecgData.length}
        />
        <form className={b('centered')}>
          <input
            // type="range"
            className={b('button')}
            type="button"
            value={'Denoise'}
            // min={0}
            // max={ecgData.length - zoomValue}
            // step={1}
            // value={start}
            // onChange={({ target }) =>
            //   this.setState({ start: Number(target.value) })
            // }
            onClick={() => {
              this.getFilteredECG(1);
              this.setState({start: 0, showDenoised: true});
              this.updateECg(ecgData, zoomValue)
            }}
          />
        </form>
        <Chart
          ecgData={filtered.slice(start, start + zoomValue)}
          rpeaks={rpeaks.filter(
            value => value >= start && value <= start + zoomValue
          )}
          sampleRate={sampleRate}
          startValue={start}
          endValue={start + zoomValue}
          yRange={filteredECGYRange}
          title={'Denoised signal'}
          totalECGLength={ecgData.length}
          visible={showDenoised}
        />
          {/*<input*/}
          {/*  type="button"*/}
          {/*  value={'Zoom Out'}*/}
          {/*  onClick={() => {*/}
          {/*    if (zoomValue + start + 100 <= ecgData.length)*/}
          {/*      this.setState({ zoomValue: zoomValue + 100 });*/}
          {/*    else if (start > 0) {*/}
          {/*      this.setState({*/}
          {/*        zoomValue: zoomValue + 100,*/}
          {/*        start: start - 100,*/}
          {/*      });*/}
          {/*    }*/}
          {/*  }}*/}
          {/*/>*/}
          {/*<input*/}
          {/*  type="button"*/}
          {/*  value={'Zoom In'}*/}
          {/*  onClick={() => {*/}
          {/*    if (zoomValue - 100 > 0)*/}
          {/*      this.setState({ zoomValue: zoomValue - 100 });*/}
          {/*  }}*/}
          {/*/>*/}
      </main>
    );
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export default App;
