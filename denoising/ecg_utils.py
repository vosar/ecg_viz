import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
# import seaborn as sns
from scipy.signal import butter, sosfilt, sosfilt_zi, sosfiltfilt, lfilter, lfilter_zi, filtfilt, sosfreqz, resample
# from ecg_detectors_master.ecgdetectors import Detectors, MWA, panPeakDetect, searchBack
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels

# sns.set()


def plot_fft(data, sample_rate):
    N = len(data)
    T = 1.0 / sample_rate
    xf = np.linspace(0.0, 2.0 / (T), N // 2) # 0, 2*sample_rate, len(data) / 2
    Y = np.fft.fft(data) / len(data)  # fft computing and normalization
    Y = Y[:N // 2] # FFT is symmetric -- take first half
    plt.plot(xf[:100], 2.0 / N * np.abs(Y[:100]))
    plt.title("DFT")
    plt.show()
    print("MEAN FOURIER", float(np.mean(Y)))
    print("STD FOURIER", float(np.std(Y)))

def read_ecg(path):
    with open(path) as f:
        ecg_data = list(map(lambda x: float(x.strip()), f.readlines()))
    return ecg_data


def plot_ecg(data1, data2, sample_rate, percentageToShow=100, saveToSVG=True):
    num = int(len(data1) * (percentageToShow / 100))

    # resampled_x = resample(x=data, num=int(len(data) * (percentageToShow / 100)))

    showData1 = data1[:num]
    showData2 = data2[:num]

    # Calculate time values in seconds
    times = np.arange(len(showData1), dtype='float') / sample_rate

    plt.figure(figsize=(20, 5))
    plt.ylabel("Amplitude (dB)")
    plt.plot(times, showData1, "r")
    plt.plot(times, showData2, "g")
    plt.xlabel("Time (s)")
    plt.title("ECG sample raw data")
    plt.legend("best")
    if saveToSVG:
        plt.savefig("figures/raw_ecg.svg")
    plt.show()


def butter_bandpass_filter_once(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass(lowcut, highcut, fs, order=order)
    # Apply the filter to data. Use lfilter_zi to choose the initial condition of the filter.
    zi = sosfilt_zi(sos)
    z, _ = sosfilt(sos, data, zi=zi * data[0])
    return sos, z, zi


def butter_bandpass_filter_again(sos, z, zi):
    # Apply the filter again, to have a result filtered at an order the same as filtfilt.
    z2, _ = sosfilt(sos, z, zi=zi * z[0])
    return z2


def butter_bandpass(lowcut, highcut, fs, order=4):
    nyq = 0.5 * fs  # Nyquist frequency is half the sampling frequency
    low = lowcut / nyq
    high = highcut / nyq
    sos = butter(order, [low, high], analog=False, btype="band", output="sos")
    return sos


def butter_bandpass_forward_backward_filter(data, lowcut, highcut, fs, order=5):
    sos = butter_bandpass(lowcut, highcut, fs, order=order)
    y = sosfiltfilt(sos,
                    data)  # Apply a digital filter forward and backward to a signal. This function applies a linear
    # digital filter twice, once forward and once backwards. The combined filter has zero phase and a filter order
    # twice that of the original.
    return y


def filter(lowcut, highcut, x, fs, plot=True):
    y = butter_bandpass_forward_backward_filter(x, lowcut, highcut, fs, order=4)

    if plot:
        # Plot the frequency response for a few different orders.
        plt.figure(1, figsize=(10, 4))
        plt.clf()
        for order in [4, 10]:
            sos = butter_bandpass(lowcut, highcut, fs, order=order)
            w, h = sosfreqz(sos, worN=2000)
            plt.plot(((fs * 0.5 / np.pi) * w)[:200], abs(h)[:200], label="order = %d" % order)
        #
        # plt.plot([0, 0.5 * fs], [np.sqrt(0.5), np.sqrt(0.5)],
        #          "--", label="sqrt(0.5)")
        plt.xlabel("Frequency (Hz)")
        plt.ylabel("Gain")
        plt.title("Frequency response for a few different orders")
        plt.grid(True)
        plt.legend(loc="best")

        # Filter a noisy signal.

        # Calculate time values in seconds
        times = np.arange(x.shape[0], dtype='float') / fs

        plt.figure(2, figsize=(20, 8))
        plt.clf()
        plt.ylabel("Amplitude (dB)")
        plt.xlabel("Time (s)")
        plt.plot(times, x, "g", linewidth=0.5, label="Noisy signal")
        plt.legend(loc="lower left")
        plt.twinx()
        plt.plot(times, y, linewidth=2.5, label="Filtered signal (%g Hz) using sosfiltfilt" % fs)

        plt.grid(True)
        plt.axis("tight")
        plt.legend(loc="upper right")
        plt.savefig("figures/filtering.svg")
        plt.show()

    return y


# def pan_tompkins_detector(raw_ecg, mwa, fs, N):
#     #     N = int(0.12 * fs)
#     #     mwa = MWA(squared, N)
#     #     mwa[:int(0.2 * fs)] = 0
#
#     N = int(N / 100 * fs)
#     mwa_peaks = panPeakDetect(mwa, fs)
#
#     r_peaks = searchBack(mwa_peaks, raw_ecg, N)
#
#     return r_peaks


def plot_pan_tompkins(data, y, differentiated_ecg_measurements, squared_ecg_measurements, integrated_ecg_measurements,
                      fs, rpeaks):
    figsize = (20, 10)

    plt.figure(figsize=figsize)

    plt.subplot2grid((6, 2), (0, 0))
    # plt.subplot(621)
    # plt.figure(1, figsize=left_figsize)
    # plt.clf()
    plt.xticks([])
    plt.plot(data)
    plt.title("Raw ecg data")
    plt.grid(True)
    # plt.show()

    # plt.figure(figsize=left_figsize)
    plt.subplot2grid((6, 2), (1, 0))
    # plt.subplot(622)
    # plt.clf()
    plt.xticks([])
    plt.plot(y)
    plt.title("Filtered ecg data")
    plt.grid(True)
    # plt.show()

    # plt.figure(figsize=left_figsize)
    plt.subplot2grid((6, 2), (2, 0))
    # plt.subplot(623)
    # plt.clf()
    plt.xticks([])
    plt.plot(differentiated_ecg_measurements)
    plt.title("Differentiated ecg data")
    plt.grid(True)
    # plt.show()

    # plt.figure(figsize=left_figsize)
    plt.subplot2grid((6, 2), (3, 0))
    # plt.subplot(624)
    # plt.clf()
    plt.xticks([])
    plt.plot(squared_ecg_measurements)
    plt.title("Squared ecg data")
    plt.grid(True)
    # plt.show()

    # plt.figure(figsize=left_figsize)
    plt.subplot2grid((6, 2), (4, 0))
    # plt.subplot(625)
    # plt.clf()
    plt.xticks([])
    plt.plot(integrated_ecg_measurements)
    plt.title("Integrated ECG data")
    plt.grid(True)
    # plt.show()

    ymin = np.min(data)
    ymax = np.max(data)
    alpha = 0.2 * (ymax - ymin)
    ymax += alpha
    ymin -= alpha

    # plt.figure(figsize=left_figsize)
    # plt.subplot(626)
    plt.subplot2grid((6, 2), (5, 0))
    # plt.clf()
    plt.ylabel("Amplitude (dB)")
    # Calculate time values in seconds
    times = np.arange(data.shape[0], dtype='float') / fs
    plt.xlabel("Time (s)")
    plt.plot(times, data)
    plt.vlines(rpeaks / fs, ymin, ymax,
               color="r",
               linewidth=2,
               label="R-peaks")
    plt.title("Raw ecg data with pulse stream")
    plt.grid(True)
    # plt.show()

    plt.subplot2grid((6, 2), (0, 1))
    pan_tompkins_fig1_a = mpimg.imread("images/a_original_signal.png")
    plt.title("Original signal - from Pan, Tompkins et al.")
    plt.axis("off")
    plt.imshow(pan_tompkins_fig1_a)

    plt.subplot2grid((6, 2), (1, 1))
    pan_tompkins_fig1_b = mpimg.imread("images/b_output_of_bandpass_filter.png")
    plt.title("Output of bandpass filter - from Pan, Tompkins et al.")
    plt.axis("off")
    plt.imshow(pan_tompkins_fig1_b)

    plt.subplot2grid((6, 2), (2, 1))
    pan_tompkins_fig1_c = mpimg.imread("images/c_ouput_of_differentiator.png")
    plt.title("Output of differentiator - from Pan, Tompkins et al.")
    plt.axis("off")
    plt.imshow(pan_tompkins_fig1_c)

    plt.subplot2grid((6, 2), (3, 1))
    pan_tompkins_fig1_d = mpimg.imread("images/d_output_of_squaring_process.png")
    plt.title("Output of Squaring process - from Pan, Tompkins et al.")
    plt.axis("off")
    plt.imshow(pan_tompkins_fig1_d)

    plt.subplot2grid((6, 2), (4, 1))
    pan_tompkins_fig1_e = mpimg.imread("images/e_results_of_moving_window_integration.png")
    plt.title("Results of moving window integration - from Pan, Tompkins et al.")
    plt.axis("off")
    plt.imshow(pan_tompkins_fig1_e)

    plt.subplot2grid((6, 2), (5, 1))
    pan_tompkins_fig1_g = mpimg.imread("images/g_output_pulse_stream.png")
    plt.title("Output pulse stream - from Pan, Tompkins et al.")
    plt.axis("off")
    plt.imshow(pan_tompkins_fig1_g)

    plt.savefig("figures/pan_tompkins.svg")
    plt.show()


def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax

