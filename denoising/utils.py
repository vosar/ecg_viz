""" Copyright 2017 SoftServe Inc.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. """

"""
Utility functions reused in various parts of the code
"""

import numpy as np
import matplotlib.pyplot as plt
import biosppy
import scipy
from ecg_utils import butter_bandpass_forward_backward_filter, butter_bandpass_filter_once, butter_bandpass_filter_again
from statsmodels.tsa.arima_model import ARMA, ARIMA


lowcut = 7.5  # ... * 60 beats per min
highcut = 50  # ... * 60 beats per min


def scale_signal(signal, old_min, old_max, new_min, new_max):
    # old_signal = np.array(old_signal)
    # example_signal = np.array(example_signal)

    OldRange = (old_max - old_min)
    NewRange = (new_max - new_min)
    # old_min = old_signal.min()
    # new_min = example_signal.min()

    new_signal = (((signal - old_min) * NewRange) / OldRange) + new_min
    return new_signal


def plot_ecg(*args):
    s = 0
    step = 1000
    n_diagrams = len(args)  # min(len(filtered_ecg) // step, 5)
    # n_ranges = sorted(np.random.choice(len(filtered_ecg) // step, size=n_diagrams, replace=False))
    # print(n_ranges)
    for s in range(s, len(args[0]), step):
        # cur_f_data = args[0][s: s + step]
        # cur_out_data = args[][s: s+step]
        # data = np.array(data)
        # noise = np.random.normal(0, 60, len(data))
        # noisy_data = data + noise
        # data = (data - np.mean(data)) / np.std(data)
        # for result in [data]:  # , filtered_ecg, scaled_filtered_ecg]:
        signals_cuts = list(map(lambda x: x[s:s+step], args))
        plt.figure()
        i = 0
        for data in signals_cuts:
            # s = n_ranges[i] * 1000
            plt.subplot(n_diagrams, 1, i + 1)
            plt.plot(data)  # [s:s+data_len])#[s:s+step])
            # plt.plot(np.zeros(len(result)))
            i += 1
        plt.title(str(s))
        plt.show()


def filter_signal(ecg_signal, freq, filtered=None):
    if filtered == 'biosppy':
        out = biosppy.signals.ecg.ecg(signal=ecg_signal, sampling_rate=freq, show=False)
        ecg_signal = out['filtered']
    elif filtered == 'bpass_fb':
        ecg_signal = butter_bandpass_forward_backward_filter(ecg_signal, lowcut, highcut, freq, order=4)
    elif filtered == 'bpass_tt':
        sos, z, zi = butter_bandpass_filter_once(ecg_signal, lowcut, highcut, freq, order=4)
        ecg_signal = butter_bandpass_filter_again(sos, z, zi)
    return ecg_signal


def signal_to_frames(ecg_signal, freq=250, signal_length=5, filtered=None):
    if signal_length > 5:
        ecg_signal = scipy.signal.resample(ecg_signal, int(signal_length * freq))

    # ecg_signal = ecg_signal / (max(ecg_signal) - min(ecg_signal))
    ecg_signal = filter_signal(ecg_signal, freq, filtered)
    # ecg_signal = butter_bandpass_forward_backward_filter(ecg_signal, lowcut, highcut, freq, order=4)
    # out = biosppy.ecg.ecg(signal=ecg_signal, sampling_rate=freq, show=False)
    # ecg_signal = out['filtered']
    frames = [ecg_signal[i:i + 200] for i in range(0, len(ecg_signal), 200)]

    # forecast
    data = frames[-1]
    frames.pop()
    # forecast = []
    # if len(frames[0]) - len(data) > 0:
    #     print(len(data))
    #     forecast = reversed(data[-(len(frames[0]) - len(data)):])
    #     print(len(list(forecast)))
    #     print(len(list(data) + list(forecast)))
    #     exit(0)
    # frames[-1] = list(data) + list(forecast)
    # model = ARIMA(ecg_signal, order=(1, 1, 1))
    # model_fit = model.fit(disp=False)
    # # make prediction
    # yhat = model_fit.predict(len(data), len(frames[0]) - 1, typ='levels')
    # frames.append(frames[-1] + yhat)

    return frames


def templates(peak_ind, data, freq=250.):
    """
    cut templates
    :param peak_ind: R-peak coordinate
    :param data: full signal
    :return: 0.2 seconds before R-peak and 0.6 after
    """
    x = []
    for i in range(len(peak_ind)):
        if peak_ind[i] - int(0.2*freq) > 0 and (peak_ind[i]+int(0.6*freq) < len(data)):
            x.append(data[peak_ind[i]-int(0.2*freq):peak_ind[i]+int(0.6*freq)])
    return np.array(x)


def rpeaks2templates(ecg_signal, rpeaks=0, freq=250, signal_length = 5):
    """
    detect R-peak coordinate
    :param db_y: signal from file
    :return: template for each signal
    """
    # print("Input", len(ecg_signal))
    # if signal_length != 5:
    #     ecg_signal = scipy.signal.resample(np.array(ecg_signal, dtype=float), int(signal_length*freq))
    # print("Resampled", len(ecg_signal))
    # ecg_signal = ecg_signal / (max(ecg_signal) - min(ecg_signal))
    if isinstance(rpeaks, int):
        ts, f, rpeaks, t_ts, t, hrs, hr = biosppy.signals.ecg.ecg(signal=ecg_signal, sampling_rate=freq, show=False)
        print("Peaks num", len(rpeaks))
        print(rpeaks[0])


    templates_list = templates(rpeaks, ecg_signal)
    # print(rpeaks)
    return rpeaks, templates_list

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]

def plot_learning_curve(train_curve, test_curve, disc_curve=0):
    import pylab as plt
    plt.figure(0)
    plt.plot(train_curve, label="train curve")
    plt.plot(test_curve, label="test curve")
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
               ncol=2, mode="expand", borderaxespad=0.)
    # plt.plot(disc_curve)
    plt.savefig('train_test_curve.pdf', format='pdf')
    plt.show()
    print("learning curve saved")


def plot_signal_eval_examples(raw_signal, filtered_signal, clean_signal):
    import pylab as plt
    from math import ceil
    num_plots = len(raw_signal)
    num_columns = 4
    num_rows = ceil(float(num_plots) / float(num_columns))

    plt.figure(figsize=(10,6))

    for i in range(1, num_plots+1):

        ax = plt.subplot(num_rows,num_columns, i)

        plt.plot(raw_signal[i-1])
        plt.plot(clean_signal[i-1])
        plt.plot(filtered_signal[i-1])
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(True)

    plt.savefig('filtered_signal.pdf', format='pdf')
    plt.show()

    print("learning curve saved")