""" Copyright 2017 SoftServe Inc.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. """
"""
Reconstruct a feed-forward network without loading theano.
"""

import numpy as np
import pickle
import io
import matplotlib.pyplot as plt
import utils
import pandas as pd
import scipy
import ecg_utils
import biosppy
from copy import deepcopy


# activation function
def activation_func(X):
    relu = [x if x > 0 else 0.01*x for x in X]
    return np.asarray(relu)

def sigmoid(z):
    return 1.0/(1.0+np.exp(-z))


def feed_forward_ae(vecX):
    net = pickle.load(open('models/network_ae.pklz', 'r'))[:]
    X_in = vecX

    # go over hidden layers
    for layer_idx in range(len(net)/2 - 1):
        W = net[2*layer_idx]
        b = net[2*layer_idx + 1]
        X_in = activation_func(np.dot(X_in, W)+b)

    W = net[-2]
    b = net[-1]
    X_in = np.dot(X_in, W)+b

    return np.asarray(X_in)


def feed_forward_gan(vecX):
    net = pickle.load(open('models/network_gan.pklz', 'r'))[:]
    X_in = vecX

    # go over hidden layers
    for layer_idx in range(len(net)/2 - 1):
        W = net[2*layer_idx] # 200x40
        b = net[2*layer_idx + 1] # 40x1
        # print(W.shape, b.shape)
        # exit(0)
        X_in = activation_func(np.dot(X_in, W)+b)

    W = net[-2]
    b = net[-1]
    X_in = np.dot(X_in, W)+b

    return np.asarray(X_in)


def denoise_templates(templates, concatenate=False):
    concat = []
    for i in range(len(templates)):
        in_data = templates[i]
        # print(f_data)
        result = feed_forward_gan(in_data)
        if concatenate:
            concat.extend(result)
        else:
            concat.append(result)
    concat = np.array(concat)
    return concat


if __name__ == "__main__":
    # with open('DATA/ecg_with_noise_test.pklz', 'r') as f:
    #     ecg_noisy = np.array(pickle.load(f))
    # print(ecg_noisy.shape)
    # ecg_idx = 100
    # data = ecg_noisy[ecg_idx]

    signal_length = 10
    freq = 512
    s = 15000
    path = "DATA/wild/ecg_oleg.log"#ECG_ID_database/Person_01_rec_5.csv"
    data = pd.read_csv(path, header=None)[s:s+5000].astype('float64')[0].values  #["'ECG I'"]
    # ecg_utils.plot_fft(data, freq)
    data = data / (max(data) - min(data))
    # out = biosppy.signals.ecg.ecg(signal=data, sampling_rate=freq, show=False)
    # f_data = data.copy()
    # f_data = utils.signal_to_frames(data, freq=freq, signal_length=signal_length)
    # f_data_filt_biosppy = utils.signal_to_frames(data, freq=freq, signal_length=signal_length, filtered='biosppy')
    # f_data_filt_bpass_fb = utils.signal_to_frames(data, freq=freq, signal_length=signal_length, filtered='bpass_fb')
    # f_data_filt_bpass_tt = utils.signal_to_frames(data, freq=freq, signal_length=signal_length, filtered='bpass_tt')
    filt_data = utils.filter_signal(data, freq, filtered='biosppy')
    filt_data_bpass_fb = utils.filter_signal(data, freq, filtered='bpass_fb')
    filt_data_bpass_tt = utils.filter_signal(data, freq, filtered='bpass_tt')
    f_data_filt = utils.rpeaks2templates(filt_data, rpeaks=1, freq=freq, signal_length=signal_length)[1]
    f_data_filt_fb = utils.rpeaks2templates(filt_data_bpass_fb, rpeaks=1, freq=freq, signal_length=signal_length)[1]
    f_data_filt_tt = utils.rpeaks2templates(filt_data_bpass_tt, rpeaks=1, freq=freq, signal_length=signal_length)[1]
    # f_data = utils.rpeaks2templates(data, rpeaks=1, freq=freq, signal_length=signal_length)[1]
    # f_data.insert(0, [0]*200)
    concat_filt = denoise_templates(f_data_filt)
    concat_filt_fb = denoise_templates(f_data_filt_fb)
    concat_filt_tt = denoise_templates(f_data_filt_tt)
    # concat = scale_signal(concat, data)
    # concat_filt_biosppy = denoise_frames(f_data_filt_biosppy)
    # concat_filt_bpass_fb = denoise_frames(f_data_filt_bpass_fb)
    # concat_filt_bpass_tt = denoise_frames(f_data_filt_bpass_tt)

    # ecg_utils.plot_fft(concat, freq)
    # print(concat.shape)
    # flat_f_data = []
    # for lst in f_data[s:s+10]:
    #     flat_f_data.extend(lst)

    # plt.figure()
    # plt.subplot(2, 1, 1)
    # plt.plot(np.linspace(0, len(data), len(data)), data)
    # plt.subplot(2, 1, 2)
    # plt.plot(np.linspace(0, len(data), len(concat)), concat)
    # plt.show()
    # plot_ecg(flat_f_data)
    # plot_ecg(data, concat_filt, concat_filt_fb, concat_filt_tt) #, concat_filt_biosppy, concat_filt_bpass_fb, concat_filt_bpass_tt)
