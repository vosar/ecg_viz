import numpy as np
import pickle
import pandas as pd
import csv
import scipy

import utils
from denoise_feedforward import denoise_templates

SIGNAL_LENGTH = 10
SAMPLE_RATE = 512


def write_to_csv(fname, data):
    with open(fname, 'w') as csv_f:
        csv_writer = csv.writer(csv_f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(data)


def denoise_ecg(data_path, filter_type, concatenate_templates=False):
    """

    :param data_path:
    :param filter_type: biosppy | bpass_fb | bpass_tt
    :param concatenate_templates: whether to merge templates into solid signal
    :return:
    """
    raw_ecg = pd.read_csv(data_path, header=None).astype('float64')[0].values
    raw_min, raw_max = np.min(raw_ecg), np.max(raw_ecg)
    raw_ecg = raw_ecg / (max(raw_ecg) - min(raw_ecg))

    filtered_ecg = utils.filter_signal(raw_ecg, SAMPLE_RATE, filtered=filter_type)
    rpeaks, ecg_templates = utils.rpeaks2templates(filtered_ecg, rpeaks=1, freq=SAMPLE_RATE,
                                                   signal_length=SIGNAL_LENGTH)

    denoised_templates = denoise_templates(ecg_templates, concatenate=concatenate_templates)
    if concatenate_templates:
        denoised_min, denoised_max = min(denoised_templates), max(denoised_templates)
        scaled_templates = list(map(lambda x: round(x, 1), utils.scale_signal(denoised_templates, denoised_min,
                                                                              denoised_max, raw_min, raw_max)))
    else:
        denoised_min, denoised_max = min(list(map(min, denoised_templates))), max(list(map(max, denoised_templates)))
        scaled_templates = [None] * len(raw_ecg)
        peak_idx = 0
        for template in denoised_templates:
            template = list(map(lambda x: round(x, 1), utils.scale_signal(template, denoised_min, denoised_max, raw_min,
                                                                          raw_max)))
            concatenate_templates_with_skips(scaled_templates, template, rpeaks[peak_idx], len(raw_ecg))
            peak_idx += 1
    return scaled_templates


def concatenate_templates_with_skips(denoised_ecg_centered, template, orig_rpeak_idx, raw_ecg_len):
    """
    Modify denoised_ecg_centered to contain denoised template
    :param denoised_ecg_centered:
    :param template:
    :param orig_rpeak_idx:
    :param raw_ecg_len:
    :return:
    """
    template_peak_idx = int(np.argmax(template))

    peak_left_side = orig_rpeak_idx - template_peak_idx
    peak_left_side = peak_left_side if peak_left_side >= 0 else 0
    peak_right_side = orig_rpeak_idx + len(template) - template_peak_idx
    peak_right_side = peak_right_side if peak_right_side < raw_ecg_len else raw_ecg_len

    denoised_ecg_centered[peak_left_side:orig_rpeak_idx] = template[:template_peak_idx]
    denoised_ecg_centered[orig_rpeak_idx:peak_right_side] = template[template_peak_idx:]


import time

if __name__ == "__main__":
    path = "DATA/wild/ecg_oleg.log"
    raw_ecg = pd.read_csv(path, header=None).astype('float64')[0].values

    s = time.time()
    denoised_ecg = denoise_ecg(path, 'biosppy', concatenate_templates=False)
    print("Len denoised", len(denoised_ecg))
    print(time.time() - s)
    write_to_csv('denoised_results/denoised_ecg_oleg_skips.csv', denoised_ecg)
    # utils.plot_ecg(raw_ecg[:5000], denoised_ecg[:5000])
